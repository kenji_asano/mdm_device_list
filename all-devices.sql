SELECT
  devices.id,
  "DeviceName",
  "OSVersion",
  "SerialNumber",
  devices.last_checkin_time,
  devices.last_push_time,
  (library_item_metadata.dynamic_attributes::json)->'ICCID' AS "ICCID",
  "IMEI",
  users.full_name,
  devices.updated_at,
  devices.created_at
FROM devices
LEFT JOIN users ON devices.user_id = users.id
LEFT JOIN library_item_metadata ON devices.id = library_item_metadata.id
ORDER BY
  updated_at DESC;
